<?php

namespace Drupal\prevnext;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;

/**
 * Main service file.
 *
 * @package Drupal\prevnext
 */
class PrevNextService implements PrevNextServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Previous / Next nids.
   *
   * @var array
   */
  public $prevnext;

  /**
   * PrevNextService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager instance.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviousNext(NodeInterface $node) {
    $this->prevnext['prev'] = $this->getNodesOfType($node, 'prev');
    $this->prevnext['next'] = $this->getNodesOfType($node, 'next');

    return $this->prevnext;
  }

  /**
   * Retrieves the prev and next nid filtered by the provided node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node entity.
   * @param string $type
   *   The type of navigation, either 'prev' or 'next'.
   *
   * @return string
   *   A prev/next nid filtered by type, status and language.
   */
  protected function getNodesOfType(NodeInterface $node, $type) {
    $bundle = $node->bundle();
    $langcode = $node->language()->getId();

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->accessCheck();
    $query->condition('status', NodeInterface::PUBLISHED);
    $query->condition('type', $bundle);
    $query->condition('langcode', $langcode);
    $query->range(0, 1);
    $query->addMetaData('type', $bundle);
    $query->addMetaData('langcode', $langcode);
    $query->addTag('prev_next_nodes_type');

    switch ($type) {
      case 'prev':
        $query->condition('nid', $node->id(), '<');
        $query->sort('nid', 'DESC');
        $query->addTag('prev_next_nodes_type_prev');
        break;

      case 'next':
        $query->condition('nid', $node->id(), '>');
        $query->sort('nid');
        $query->addTag('prev_next_nodes_type_next');
        break;

    }

    $id = '';
    if ($results = $query->execute()) {
      $id = reset($results);
    }

    return $id;
  }

}
