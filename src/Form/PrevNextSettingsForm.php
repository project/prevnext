<?php

namespace Drupal\prevnext\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for the module settings.
 *
 * @package Drupal\prevnext\Form
 */
class PrevNextSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prevnext_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['prevnext.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('prevnext.settings');

    $form['prevnext_enabled_nodetypes'] = [
      '#title' => $this->t('Enabled Node Types'),
      '#description' => $this->t('Check node types enabled for PrevNext'),
      '#type' => 'checkboxes',
      '#options' => node_type_get_names(),
      '#default_value' => !empty($config->get('prevnext_enabled_nodetypes')) ? $config->get('prevnext_enabled_nodetypes') : [],
    ];

    $form['prevnext_premission_check'] = [
      '#title' => $this->t('Use permission check to view PrevNext links'),
      '#type' => 'checkbox',
      '#default_value' => !empty($config->get('prevnext_premission_check')) ? $config->get('prevnext_premission_check') : FALSE,
    ];

    $form['use_node_titles'] = [
      '#title' => $this->t('Show node titles as PrevNext links'),
      '#type' => 'checkbox',
      '#default_value' => !empty($config->get('use_node_titles')) ? $config->get('use_node_titles') : FALSE,
    ];

    $form['prev_link_prefix'] = [
      '#title' => $this->t('Previous link prefix'),
      '#type' => 'textfield',
      '#default_value' => !empty($config->get('prev_link_prefix')) ? $config->get('prev_link_prefix') : '',
      '#states' => [
        'visible' => [
          ':input[name="use_node_titles"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['next_link_suffix'] = [
      '#title' => $this->t('Next link suffix'),
      '#type' => 'textfield',
      '#default_value' => !empty($config->get('next_link_suffix')) ? $config->get('next_link_suffix') : '',
      '#states' => [
        'visible' => [
          ':input[name="use_node_titles"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the config values.
    $this->config('prevnext.settings')
      ->set('prevnext_enabled_nodetypes', $form_state->getValue('prevnext_enabled_nodetypes'))
      ->set('prevnext_premission_check', (bool) $form_state->getValue('prevnext_premission_check'))
      ->set('use_node_titles', (bool) $form_state->getValue('use_node_titles'))
      ->set('prev_link_prefix', $form_state->getValue('prev_link_prefix'))
      ->set('next_link_suffix', $form_state->getValue('next_link_suffix'))
      ->save();

    Cache::invalidateTags(['entity_field_info']);
    parent::submitForm($form, $form_state);
  }

}
