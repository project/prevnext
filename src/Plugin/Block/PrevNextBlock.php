<?php

namespace Drupal\prevnext\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\prevnext\PrevNextServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Block with the Previous and Next links.
 *
 * @Block(
 *   id = "prevnext_block",
 *   admin_label = @Translation("PrevNext links"),
 *   category = @Translation("Other"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", required = TRUE, label = @Translation("Node"))
 *   }
 * )
 */
class PrevNextBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Returns the prevnext.service service.
   *
   * @var \Drupal\prevnext\PrevNextServiceInterface
   */
  protected $prevnext;

  /**
   * Returns the config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a PrevNextBlock block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\prevnext\PrevNextServiceInterface $prevnext
   *   Interface for the main PrevNext service file.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the interface for a configuration object factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PrevNextServiceInterface $prevnext, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->prevnext = $prevnext;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('prevnext.service'),
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->getContextValue('node');

    if (!empty($node->in_preview)) {
      return $build;
    }

    $config = $this->configFactory->get('prevnext.settings');
    $node_types = $config->get('prevnext_enabled_nodetypes');

    if (empty($node_types[$node->getType()])) {
      return $build;
    }

    $previous_next = $this->prevnext->getPreviousNext($node);
    $cache = [
      'contexts' => [
        'url',
        'user.permissions',
      ],
      'tags' => [
        'config:prevnext.settings',
        'node_list',
      ],
    ];

    $previous = $this->t('Previous');
    if ($config->get('use_node_titles') &&
      $previous_node = $this->entityTypeManager->getStorage('node')->load($previous_next['prev'])
    ) {
      $previous = $previous_node->label();

      if ($prefix = $config->get('prev_link_prefix')) {
        $previous = "{$prefix} {$previous}";
      }
    }

    $build['prevnext_previous'] = [
      '#theme' => 'prevnext',
      '#direction' => 'previous',
      '#text' => $previous,
      '#nid' => $previous_next['prev'],
      '#url' => Url::fromUserInput('/node/' . $previous_next['prev'])->toString(),
      '#void' => empty($previous_next['prev']),
      '#cache' => $cache,
    ];

    $next = $this->t('Next');
    if ($config->get('use_node_titles') &&
      $next_node = $this->entityTypeManager->getStorage('node')->load($previous_next['next'])
    ) {
      $next = $next_node->label();

      if ($suffix = $config->get('next_link_suffix')) {
        $next = "{$next} {$suffix}";
      }
    }

    $build['prevnext_next'] = [
      '#theme' => 'prevnext',
      '#direction' => 'next',
      '#text' => $next,
      '#nid' => $previous_next['next'],
      '#url' => Url::fromUserInput('/node/' . $previous_next['next'])->toString(),
      '#void' => empty($previous_next['next']),
      '#cache' => $cache,
    ];

    $build['#cache']['tags'][] = 'prevnext-' . $node->bundle();

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    $config = $this->configFactory->get('prevnext.settings');

    if ($config->get('prevnext_premission_check') &&
      !$account->hasPermission('view prevnext links')
    ) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

}
