<?php

namespace Drupal\prevnext\Plugin\views\field;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\prevnext\PrevNextServiceInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide display for the Previous and Next links.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("prevnext_links_field")
 */
class PrevNextLinks extends FieldPluginBase {

  /**
   * Returns the prevnext.service service.
   *
   * @var \Drupal\prevnext\PrevNextServiceInterface
   */
  protected $prevnext;

  /**
   * Returns the config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Returns the current_user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PrevNextLinks instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\prevnext\PrevNextServiceInterface $prevnext
   *   Interface for the main PrevNext service file.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the interface for a configuration object factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Defines an account interface which represents the current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PrevNextServiceInterface $prevnext, ConfigFactoryInterface $config_factory, AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->prevnext = $prevnext;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('prevnext.service'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {}

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $build = [];

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->getEntity($values);

    if (!$node) {
      return $build;
    }

    $config = $this->configFactory->get('prevnext.settings');
    $node_types = $config->get('prevnext_enabled_nodetypes');

    if (empty($node_types[$node->getType()])) {
      return $build;
    }

    if ($config->get('prevnext_premission_check') &&
      !$this->currentUser->hasPermission('view prevnext links')
    ) {
      return $build;
    }

    $previous_next = $this->prevnext->getPreviousNext($node);
    $cache = [
      'contexts' => [
        'url',
        'user.permissions',
      ],
      'tags' => [
        'config:prevnext.settings',
        'node_list',
      ],
    ];

    $previous = $this->t('Previous');
    if ($config->get('use_node_titles') &&
      $previous_node = $this->entityTypeManager->getStorage('node')->load($previous_next['prev'])
    ) {
      $previous = $previous_node->label();

      if ($prefix = $config->get('prev_link_prefix')) {
        $previous = "{$prefix} {$previous}";
      }
    }

    $build['prevnext_previous'] = [
      '#theme' => 'prevnext',
      '#direction' => 'previous',
      '#text' => $previous,
      '#nid' => $previous_next['prev'],
      '#url' => Url::fromUserInput('/node/' . $previous_next['prev'])->toString(),
      '#void' => empty($previous_next['prev']),
      '#cache' => $cache,
    ];

    $next = $this->t('Next');
    if ($config->get('use_node_titles') &&
      $next_node = $this->entityTypeManager->getStorage('node')->load($previous_next['next'])
    ) {
      $next = $next_node->label();

      if ($suffix = $config->get('next_link_suffix')) {
        $next = "{$next} {$suffix}";
      }
    }

    $build['prevnext_next'] = [
      '#theme' => 'prevnext',
      '#direction' => 'next',
      '#text' => $next,
      '#nid' => $previous_next['next'],
      '#url' => Url::fromUserInput('/node/' . $previous_next['next'])->toString(),
      '#void' => empty($previous_next['next']),
      '#cache' => $cache,
    ];

    $build['#cache']['tags'][] = 'prevnext-' . $node->bundle();

    return $build;
  }

}
