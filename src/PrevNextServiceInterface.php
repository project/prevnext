<?php

namespace Drupal\prevnext;

use Drupal\node\NodeInterface;

/**
 * Interface for the main service file.
 *
 * @package Drupal\prevnext
 */
interface PrevNextServiceInterface {

  /**
   * Retrieves previous and next nids of a given node, if they exist.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node entity.
   *
   * @return array
   *   An array of prev/next nids of given node.
   */
  public function getPreviousNext(NodeInterface $node);

}
