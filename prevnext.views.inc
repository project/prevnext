<?php

/**
 * @file
 * Contains prevnext.views.inc.
 */

/**
 * Implements hook_views_data_alter().
 */
function prevnext_views_data_alter(array &$data) {
  $data['node']['prevnext_links_field'] = [
    'title' => t('Previous/Next links'),
    'group' => t('Content'),
    'field' => [
      'title' => t('Previous/Next links'),
      'id' => 'prevnext_links_field',
    ],
  ];
}
